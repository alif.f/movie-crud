class Movie {
  final int? id;
  final String? title;
  final String? director;
  final String? summary;
  final List<String>? tags;

  Movie({required this.id, required this.title, required this.director, required this.summary, required this.tags});
}