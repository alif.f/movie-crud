import 'dart:math';

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:moviecrud/data/models/movie.dart';
import 'package:provider/provider.dart';

import '../list/store/movie_store.dart';

class FormMovieScreen extends StatefulWidget {
  final bool isEditMode;
  final int? index;

  const FormMovieScreen(
      {Key? key, required this.isEditMode, required this.index})
      : super(key: key);

  @override
  State<FormMovieScreen> createState() => _FormMovieScreenState();
}

class _FormMovieScreenState extends State<FormMovieScreen> {
  String selectedValue = "Action";
  List<String> tags = [];
  final _titleController = TextEditingController();
  final _directorController = TextEditingController();
  final _summaryController = TextEditingController();
  late MovieStore movieStore;

  @override
  void initState() {
    super.initState();
    if (!widget.isEditMode && widget.index == null) return;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    movieStore = Provider.of<MovieStore>(context);
  }

  @override
  void dispose() {
    _titleController.dispose();
    _directorController.dispose();
    _summaryController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Form Movie'), actions: [
        Padding(
            padding: const EdgeInsets.all(20.0),
            child: GestureDetector(
                onTap: () {
                  _deleteMovie();
                },
                child: const Text('DELETE'))),
      ],),
      body: Observer(
        builder: (BuildContext context) {
          if (!widget.isEditMode) {
            return _buildForm();
          } else {
            if (movieStore.movies.isEmpty) {
              return _buildForm();
            } else {
              _setMovieData(movieStore.movies.elementAt(widget.index!));
              return _buildForm();
            }
          }
        },
      ),
    );
  }

  List<DropdownMenuItem<String>> get dropdownItems {
    List<DropdownMenuItem<String>> menuItems = [
      const DropdownMenuItem(child: Text("Action"), value: "Action"),
      const DropdownMenuItem(child: Text("Comedy"), value: "Comedy"),
      const DropdownMenuItem(child: Text("Horror"), value: "Horror"),
      const DropdownMenuItem(child: Text("Drama"), value: "Drama"),
      const DropdownMenuItem(child: Text("Anime"), value: "Anime"),
      const DropdownMenuItem(child: Text("Sci-fi"), value: "Sci-fi"),
    ];
    return menuItems;
  }

  void _addMovie() {
    Random random = Random();
    int randomNumber = random.nextInt(1000);

    final movie = Movie(
        id: randomNumber,
        title: _titleController.text,
        director: _directorController.text,
        tags: tags,
        summary: _summaryController.text);

    movieStore.addMovie(movie);

    context.router.pop();
  }

  void _setMovieData(Movie? movie) {
    _titleController.text = movie?.title ?? '';
    _directorController.text = movie?.director ?? '';
    _summaryController.text = movie?.summary ?? '';
    tags = movie?.tags ?? [];
  }

  Widget _buildForm() {
    return SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextFormField(
              controller: _titleController,
              decoration: const InputDecoration(
                hintText: 'please type movie title..',
                labelText: 'Title',
              ),
              onSaved: (String? value) {
                // This optional block of code can be used to run
                // code when the user saves the form.
              },
              validator: (String? value) {
                return value!.isEmpty ? 'This field cannot be empty' : null;
                // return (value != null && value.contains('@')) ? 'Do not use the @ char.' : null;
              },
            ),
            const SizedBox(height: 16),
            TextFormField(
              controller: _directorController,
              decoration: const InputDecoration(
                hintText: 'please type movie director..',
                labelText: 'Director',
              ),
              onSaved: (String? value) {
                // This optional block of code can be used to run
                // code when the user saves the form.
              },
              validator: (String? value) {

              },
            ),
            const SizedBox(height: 16),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text('Tags'),
                TextButton(
                    onPressed: () {
                      if (tags.contains(selectedValue)) {
                        return;
                      } else {
                        setState(() {
                          tags.add(selectedValue);
                        });
                      }
                    },
                    child: const Text('Add Tag'))
              ],
            ),
            const SizedBox(height: 8),
            SizedBox(
              width: double.infinity / 2,
              child: DropdownButton(
                value: selectedValue,
                items: dropdownItems,
                isExpanded: true,
                onChanged: (String? value) {
                  setState(() {
                    selectedValue = value!;
                  });
                },
              ),
            ),
            Wrap(
              spacing: 3,
              children: tags.map((e) {
                return InputChip(
                  label: Text(e),
                  onDeleted: () => setState(() {
                    tags.remove(e);
                  }),
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                );
              }).toList(),
            ),
            const SizedBox(height: 8),
            TextFormField(
              controller: _summaryController,
              decoration: const InputDecoration(
                hintText: 'please type movie summary..',
                labelText: 'Summary',
              ),
              keyboardType: TextInputType.multiline,
              maxLines: 2,
              onSaved: (String? value) {
                // This optional block of code can be used to run
                // code when the user saves the form.
              },
              validator: (String? value) {
                // return (value != null && value.contains('@')) ? 'Do not use the @ char.' : null;
              },
            ),
            const SizedBox(height: 16),
            SizedBox(
              width: double.infinity,
              child: ElevatedButton(
                  onPressed: () {
                    if (widget.isEditMode) {
                      _updateMovie();
                    } else {
                      _addMovie();
                    }
                  },
                  child: Text(widget.isEditMode ? 'UPDATE' : 'ADD')),
            )
          ],
        ),
      ),
    );
  }

  void _deleteMovie() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Information'),
          content: const Text('Are you sure want to delete this movie ?'),
          actions: <Widget>[
            TextButton(
              child: const Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: const Text('Delete'),
              onPressed: () {
                movieStore.deleteMovie(widget.index!);

                Navigator.of(context).pop();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void _updateMovie() {
    final movie = Movie(
        id: movieStore.movie?.id,
        title: _titleController.text,
        director: _directorController.text,
        tags: tags,
        summary: _summaryController.text);

    movieStore.updateMovie(widget.index!, movie);
    context.router.pop();
  }
}
