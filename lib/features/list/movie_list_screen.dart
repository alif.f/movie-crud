import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobx/src/api/observable_collections.dart';
import 'package:moviecrud/core/router/app_router.gr.dart';
import 'package:moviecrud/data/models/movie.dart';
import 'package:moviecrud/features/list/store/movie_store.dart';
import 'package:provider/provider.dart';

class MovieListScreen extends StatefulWidget {
  const MovieListScreen({Key? key}) : super(key: key);

  @override
  State<MovieListScreen> createState() => _MovieListScreenState();
}

class _MovieListScreenState extends State<MovieListScreen> {
  late MovieStore movieStore;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    movieStore = Provider.of<MovieStore>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Movie'),
          actions: [
            Padding(
                padding: const EdgeInsets.all(20.0),
                child: GestureDetector(
                    onTap: () {
                      context.router.push(FormMovieScreen(isEditMode: false, index: null));
                    },
                    child: const Text('NEW MOVIE'))),
          ],
        ),
        body: Container(
          padding: const EdgeInsets.all(16),
          child: Observer(
            builder: (BuildContext context) {
              switch (movieStore.state) {
                case StoreState.initial:
                  return const Center(child: Text('There is no movies'));
                case StoreState.loading:
                  return Container();
                case StoreState.loaded:
                  return _listMovie(movieStore.movies);
              }
            },
          ),
        ));
  }

  Widget _listMovie(ObservableList<Movie> movies) {
    return ListView.builder(
      shrinkWrap: true,
      physics: const ScrollPhysics(),
      itemBuilder: (context, index) {
        return _buildMovieItem(context, movies[index], index);
      },
      itemCount: movies.length,
    );
  }

  Widget _buildMovieItem(BuildContext context, Movie movie, int index) {
    return GestureDetector(
      onTap: () {
        context.router.push(FormMovieScreen(isEditMode: true, index: index));
      },
      child: Card(
        elevation: 4,
        child: ListTile(
          title: Text(movie.title ?? '-'),
          subtitle: Text(movie.summary ?? '-'),
        ),
      ),
    );
  }
}
