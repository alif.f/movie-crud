import 'package:mobx/mobx.dart';
import 'package:moviecrud/data/models/movie.dart';

part 'movie_store.g.dart';

enum StoreState { initial, loading, loaded }

class MovieStore = _MovieStore with _$MovieStore;

abstract class _MovieStore with Store {
  @observable
  ObservableList<Movie> movies = ObservableList<Movie>();

  @observable
  Movie? movie;

  @computed
  StoreState get state {
    if (movies.isEmpty) {
      return StoreState.initial;
    }
    return StoreState.loaded;
  }

  @action
  void addMovie(Movie movie) {
    movies.add(movie);
  }

  @action
  void deleteMovie(int index) {
    movies.removeAt(index);
  }

  @action
  void updateMovie(int index, Movie movie) {
    movies[index] = movie;
  }
}