import 'package:auto_route/auto_route.dart';
import 'package:moviecrud/features/form/form_movie_screen.dart';
import 'package:moviecrud/features/list/movie_list_screen.dart';

@MaterialAutoRouter(
  replaceInRouteName: 'Page,Route',
  routes: <AutoRoute>[
    AutoRoute(page: MovieListScreen, initial: true),
    AutoRoute(page: FormMovieScreen)
  ],
)
// extend the generated private router
class $AppRouter {}