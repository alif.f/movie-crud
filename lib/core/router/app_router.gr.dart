// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i3;
import 'package:flutter/material.dart' as _i4;

import '../../features/form/form_movie_screen.dart' as _i2;
import '../../features/list/movie_list_screen.dart' as _i1;

class AppRouter extends _i3.RootStackRouter {
  AppRouter([_i4.GlobalKey<_i4.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i3.PageFactory> pagesMap = {
    MovieListScreen.name: (routeData) {
      return _i3.MaterialPageX<dynamic>(
        routeData: routeData,
        child: const _i1.MovieListScreen(),
      );
    },
    FormMovieScreen.name: (routeData) {
      final args = routeData.argsAs<FormMovieScreenArgs>();
      return _i3.MaterialPageX<dynamic>(
        routeData: routeData,
        child: _i2.FormMovieScreen(
          key: args.key,
          isEditMode: args.isEditMode,
          index: args.index,
        ),
      );
    },
  };

  @override
  List<_i3.RouteConfig> get routes => [
        _i3.RouteConfig(
          MovieListScreen.name,
          path: '/',
        ),
        _i3.RouteConfig(
          FormMovieScreen.name,
          path: '/form-movie-screen',
        ),
      ];
}

/// generated route for
/// [_i1.MovieListScreen]
class MovieListScreen extends _i3.PageRouteInfo<void> {
  const MovieListScreen()
      : super(
          MovieListScreen.name,
          path: '/',
        );

  static const String name = 'MovieListScreen';
}

/// generated route for
/// [_i2.FormMovieScreen]
class FormMovieScreen extends _i3.PageRouteInfo<FormMovieScreenArgs> {
  FormMovieScreen({
    _i4.Key? key,
    required bool isEditMode,
    required int? index,
  }) : super(
          FormMovieScreen.name,
          path: '/form-movie-screen',
          args: FormMovieScreenArgs(
            key: key,
            isEditMode: isEditMode,
            index: index,
          ),
        );

  static const String name = 'FormMovieScreen';
}

class FormMovieScreenArgs {
  const FormMovieScreenArgs({
    this.key,
    required this.isEditMode,
    required this.index,
  });

  final _i4.Key? key;

  final bool isEditMode;

  final int? index;

  @override
  String toString() {
    return 'FormMovieScreenArgs{key: $key, isEditMode: $isEditMode, index: $index}';
  }
}
